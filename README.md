# sonoj.org

Website builder and resources for https://sonoj.org .
Does not include dynamic web-programs like the challenge software (yet)

The websites needs to be build with a shell script.
* `./generate.sh 2023` will build the 2023 website and put it into build/2023, but not as part of git. Point a local webserver to build/2023 to view the result.
* `./autogenerate.sh 2023` starts a local PHP webserver and watches src files for changes and rebuilds. needs the programs `php` and `entr`
* `./upload.sh 2023` will upload the 2023 website to sonoj.org, if you have the correct local ssh credentials.

### How to create a new year

In our example we create the year 2024.

```
cp -r src/2023 src/2024
#optional in another terminal: ./autogenerate.sh 2024
```

Now do these steps. Edit the files in `src/2024/`
Line numbers may change over the years without anyone updating them here.  But they will still be nearby.

* Edit `resources/sonoj-social-media-preview.xcf` with GIMP to the next date. Export as `resources/logos/sonoj-social-media-preview.jpg`
* `pre.bottom.html`
  * line 39: Imprint copyright date
* `pre.top.html`
  * Metadata and Title
  * line 54 and 55 comment and logo alt text
  * line 84: menu link for video feed is direct link to e.g. `https://streaming.media.ccc.de/sonoj2024`
* `pre.index.html`
  * line 6: date in the banner image
  * line 83: "when and where"
  * line 102: "live video feed" to e.g. `https://streaming.media.ccc.de/sonoj2024`
* `pre.schedule.html`
  * Check if there is summer time or standard time this year, change "All times are given in ..." accordingly
  * the schedule table should contain no specific speakers yet, but generic events like "welcome", "group photo" and "get-together" can stay, as well as "Block A B C"
* `pre.donate.html`
  * Line 66: use last year as example
* `pre.event.html`
  * First sentence is "The fourth annual..." and ends in a date.
* `pre.faq.html`
  * "When?"
* `pre.speakers.html`
  * line 4 h1 title "Speakers 2024"
  * "You could be one of them" comment in or out
* Sites without changes: `pre.register.html`, `pre.contact.html`


Upload from the git root directory:
```
./generate.sh 2024
./upload.sh 2024
```

### Make a year "live".

If you want the year 2024 to be the live year on the server, change the .htaccess file in the sonoj.org html-root.
Follow the comments.
There is a copy of this file in this git `resources/` directory.

On the live server copy 2024/logos  to /logos  for some reasons. Especially the social media preview file (see above). This needs a smarter solution, but for now the files are duplicated.


### How to leave a year in the "forever" state

You need to manually go through the sourcefiles again and bring everything into the state
where no live interaction is possible anymore, for example the registration forms.
Follow the HTML comments in the files.

Then upload one last time and leave the result untouched in e.g. https://sonoj.org/2017/ .
Backup of the old finalized websites is not responsibility of this git repository.

Then edit the .htaccess file in the sonoj.org html-root. See above.

## Archive

The Sonoj Archive https://sonoj.org/archive/ is it's own directory `/archive/` with it's own build and upload script.

* `cd archive`
* `./generate.sh` will build the archive sites
* `./upload.sh` will upload the archive to sonoj.org/archive/, if you have the correct local ssh credentials.

Some files are symlinks to `/resources`. `./upload.sh` will use `rsync --copy-links` to upload the actual data.


## Large File Storage

Since we host large files, like music sessions, in the archive it needs git large file storage for
codeberg. git lfs is detected by codeberg, you or other users don't need to setup anything on codeberg,
just in the local repositories.


Install git-lfs on your system.  https://git-lfs.com/
Go into the git root


Check for potential files:
`git lfs migrate info --everything`

Track large files

```
git lfs install
git lfs track "*.ardour-session-archive"
git lfs track "*.zip"
git lfs track "*.wav"
git lfs track "*.flac"
git lfs track "*.tar.gz"
git add .gitattributes
```

Now the usual git stuff:

```
git add mysong.ardour-session-archive
git commit -m "Add ardour project"
git push
```

If you accidentally commited files before adding them to the lfs tracker you need git lfs migrate:
https://codeberg.com/git-lfs/git-lfs/blob/main/docs/man/git-lfs-migrate.adoc?utm_source=gitlfs_site&utm_medium=doc_man_migrate_link&utm_campaign=gitlfs


Convert the existing files:
`git lfs migrate import --everything --include="*.ardour-session-archiv"`
