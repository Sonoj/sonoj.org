#!/bin/bash

for FILE in pre.index.html pre.2017.html pre.2018.html pre.2019.html pre.2023.html
do
    OUT=${FILE#$"pre."}
    cat pre.top.html $FILE pre.bottom.html > $OUT
done

echo "Call upload.sh now."
