[//]: # (build with: markdown -f autolink rules-pre.md > rules-pre.html && cat rules-header.txt rules-pre.html rules-footer.txt > rules.html )

# The Sonoj Music Producing Challenge

The contest consists of three phases:

1. Introduction (~10min)
1. Make Music (1 hour)
1. Listening and Voting (~50min)

### Introduction
We explain the rules and show you where to download the sample .wav file.
Also we show you how to upload your result afterwards.

### Rules for the music making phase
1. You have one hour to create a piece of music (in the broadest sense)
1. The music must incorporate the sample, a short .wav file revealed and provided by the organizers during the introduction
1. The resulting tune should be no longer than 1 (one) minute
1. At least one open source music program of your choice must be used in an important step (see recommendations below)
1. You can prepare at home and practise. Empty sessions with your favourite programs, instruments and effects already loaded are allowed.
1. The end result should be rendered to flac or wav and uploaded to a provided location
1. The file name of your entry should be "Artist\_Title". Spaces in the filename are ok, but use only one underscore please. We will use that to automatically split composer and track.
1. You can use all of the following (and more): Midi Hardware, Hardware Synthesizers etc., Recording with a microphone, Sample Libraries (your own or third party)
1. Please keep in mind that the spirit of the event is to produce something of your own in an hour. Preparing, or buying, a one minute piece of music at home and simply putting the sample on top is no fun. Try to do everything within the hour.

### Listening and Voting
Tunes will be played in a random order. The track title will be announced, but not the author.

While listening you can give a vote from 0 to 5 (5 is best,  3 is default and means 'indifferent') on an automatically generated web form. It's URL will be revealed in time.
Voting for you own track is allowed.

After voting the results will be immediately available and announced. We will then interview the winner, and give them a chance to talk about the software they used and how they approached writing the tune.


### Recommendations
Even if you have never used open source music software or in fact any such software you should still participate.


#### General recommendations and Base Programs

* For beginners: http://libremusicproduction.com/workflow
* Ardour http://www.ardour.org , a traditional DAW setup that requires further instrument and audio effect plugins
* https://lmms.io/ LMMS for a more all-in-one approach
* http://www.audacityteam.org/ Audacity for a so called "destructive" workflow, aka. multi track audio editing. It is still fun but will not yield the most "musical" results.
* Carla Plugin Host https://kx.studio/Applications:Carla , Carla is an audio plugin host, with support for many audio drivers and plugin formats.
* Non-Session-Manager http://non.tuxfamily.org/wiki/Non%20Session%20Manager , a program for linux to remember which programs were in your session and start, stop and save them all at once.

#### Workflow ideas

You are not required to create music from the sample, just with the sample, using it once is technically enough.

You should practice before the convention by learning ways to use .wav files musically. It is allowed to create project files and prepared empty(!) sessions on your computer to quickly be able to start. Here are some ideas: 

* Use the whole sample, or parts of it, as sound effect.
* Pitch Shift (see below) and build melodies or rhythms
* Use as 'arbitrary waveform' for special synthesizers
* Create atmospheric backdrops or whole ambient soundscapes through timestreching, reverb, reverse, filter automation etc.

#### Programs to handle sample files

You can use any additional programs, as long as they are open source software, as well as resources (samples etc.). 

However, here are some programs that are specifically made to handle short sample files. There may be more.

* Audacity as general purpose destructive editor. Especially to cut. Includes Paulstretch as last resort :)
  * https://www.audacityteam.org/
* Load the .wav into your DAW and manually duplicate and arrange. This is a lot of work, better use the following:
* Ardour has a tool called Rhythm Ferret to "speed up the usually labor intensive task of slicing and adjusting a sound region to match a specific time grid."
* Ninjas 2- a sample slicer audio plugin: quick slicing of sample and mapping slices to midi note numbers.
  * https://github.com/rghvdberg/ninjas2
* samplv1 polyphonic sampler. Can load a single sound file and makes it playable by midi notes, including pitch shifting. Has some filters as well.
  * https://samplv1.sourceforge.io
* drumkv1 drum-kit sampler. Loads multiple sound files and assign them to midi notes, play back without shift pitching. + some filters.
  * https://drumkv1.sourceforge.io
* Petri-Foo a sampler. Same principle as above: load sound file, connect to midi. Has slightly different playback modes effects/filters compared to samplv1, but overall comparable feature-set.
  * https://github.com/petri-foo/Petri-Foo
