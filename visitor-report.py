#!/usr/bin/env python3

import argparse
import subprocess
import sqlite3
import os
import sys
from datetime import datetime

def copy_database(ssh_host, remote_db_path, local_db_path):
    """
    Copies the SQLite database file from the remote server to the local machine.
    """
    scp_command = ['scp', f'{ssh_host}:{remote_db_path}', local_db_path]
    try:
        subprocess.check_call(scp_command)
    except subprocess.CalledProcessError as e:
        print(f"Error copying database file: {e}")
        sys.exit(1)

def get_registrations(db_path, convention_year, sort_by, filter_duplicates):
    """
    Retrieves registrations from the database for the specified convention year,
    sorted and filtered according to the provided parameters.
    """
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    # Build the query
    query = f"""
    SELECT name, email, registration_date
    FROM registrations
    WHERE convention_year = ?
    """

    params = [convention_year]

    # Execute the query
    cursor.execute(query, params)
    rows = cursor.fetchall()

    # Convert registration_date to datetime objects
    for i, row in enumerate(rows):
        registration_date = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
        rows[i] = (row[0], row[1], registration_date)

    # Filter duplicates if requested
    if filter_duplicates:
        key_index = 0 if filter_duplicates == 'name' else 1
        seen = set()
        unique_rows = []
        for row in rows:
            key = row[key_index]
            if key not in seen:
                seen.add(key)
                unique_rows.append(row)
        rows = unique_rows

    # Sort the results
    if sort_by == 'name':
        rows.sort(key=lambda x: x[0].lower())
    elif sort_by == 'email':
        rows.sort(key=lambda x: x[1].lower())
    elif sort_by == 'registration_date':
        rows.sort(key=lambda x: x[2])

    conn.close()
    return rows

def count_unique_visitors(rows):
    """
    Counts the total number of unique visitors based on name and email.
    """
    unique_visitors = set()
    for row in rows:
        # Use a tuple of (name, email) as the unique identifier
        unique_key = (row[0].strip().lower(), row[1].strip().lower())
        unique_visitors.add(unique_key)
    return len(unique_visitors)
def main():
    parser = argparse.ArgumentParser(description='Retrieve convention registrations from SQLite database.')
    parser.add_argument('convention_year', type=str, help='convention year to retrieve registrations for.')
    parser.add_argument('--sort-by', choices=['name', 'email', 'registration_date'], default='registration_date',
                        help='Field to sort the results by.')
    parser.add_argument('--filter-duplicates', choices=['name', 'email'], help='Filter duplicates based on this field.')
    parser.add_argument('--remote-db-path', type=str, default='/home/mvsoyrjv/sonoj-visitor-registrations.sqlite',
                        help='Path to the SQLite database file on the remote server.')
    parser.add_argument('--local-db-path', type=str, default='temp-visitor-database.sqlite',
                        help='Path to store the local copy of the database file.')
    parser.add_argument('--ssh-host', type=str, default='sonoj', help='SSH host configured in your SSH config file.')

    args = parser.parse_args()

    # Copy the database file from the remote server
    copy_database(args.ssh_host, args.remote_db_path, args.local_db_path)

    # Retrieve registrations
    registrations = get_registrations(args.local_db_path, args.convention_year, args.sort_by, args.filter_duplicates)

    # Count unique visitors
    total_unique_visitors = count_unique_visitors(registrations)

    # Print the results
    print(f"\nTotal Unique Visitors for {args.convention_year}: {total_unique_visitors}\n")
    for reg in registrations:
        print(f"{reg[2].strftime('%Y-%m-%d %H:%M:%S'): <25} {reg[0]: <25} {reg[1]: <25} ")

    # Remove the local copy of the database file
    os.remove(args.local_db_path)

if __name__ == '__main__':
    main()
