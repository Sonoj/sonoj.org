#!/bin/bash

if [[ -z "$1" ]]; then
    echo "Must provide a year as parameter" 1>&2
    exit 1
fi


YEAR=$1

echo "Creating output directory: build/$YEAR/"
mkdir -p "build/$YEAR"

echo "Copying logo files"
cp --update -r resources/logos "build/$YEAR/"

echo "Copying static files"
cp --update -r static/* "build/$YEAR/"


echo "Concatenating website blocks to full html pages"
cd "src/$YEAR"
for FILE in pre.index.html pre.speakers.html pre.contact.html pre.register.html pre.schedule.html pre.event.html pre.faq.html pre.donate.html
do
    OUT=${FILE#$"pre."}
    cat pre.top.html $FILE pre.bottom.html > "../../build/$YEAR/$OUT"
    #sed needs double quote because $OUT will not be expanded with single quote
    sed -i "s/<li><a href=\"$OUT/<li class=\"active\"><a href=\"$OUT/g" "../../build/$YEAR/$OUT"
done
