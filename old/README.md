These directories and files are no longer in use.
They were mostly one-time events.
Nevertheless, it doesn't hurt to archive them.
