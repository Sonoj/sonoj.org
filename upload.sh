#!/bin/bash
#This needs "sonoj" in ~./ssh/config

if [[ -z "$1" ]]; then
    echo "Must provide a year as parameter" 1>&2
    exit 1
fi

YEAR=$1

echo Uploading year $YEAR
rsync --verbose -ru build/$YEAR/* sonoj:~/html/$YEAR/
