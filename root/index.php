<?php
// Start a session to store local login state for this page
session_start();

require __DIR__ . '/vendor/autoload.php';
include __DIR__ . '/config.php';

use Jumbojett\OpenIDConnectClient;

$oidc = new OpenIDConnectClient(
    $oidc_url,
    $oidc_client_id,
    $oidc_client_secret
);

// Set the redirect URL
$oidc->setRedirectURL($oidc_redirect_url);

// Function to set a secure, long-lived cookie
function setLoginCookie($name, $value, $expire = 86400 * 30) { // 30 days
    setcookie($name, $value, time() + $expire, "/", "", true, true);
}

// Check if the user is already logged in by checking the session or the cookie
if (isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] === true) {
    $userLoggedIn = true;
} elseif (isset($_COOKIE['user_logged_in']) && $_COOKIE['user_logged_in'] === 'true') {
    $_SESSION['user_logged_in'] = true;
    $_SESSION['user_name'] = $_COOKIE['user_name'];
    $userLoggedIn = true;
} else {
    $userLoggedIn = false;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Sonoj</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="music, production, recording, open, source, software, free, programming, freeware, linux, windows, osx, mac, tool, ardour, audacity, synthesizer, sampler, midi">
    <meta name="robots" content="index, follow">
    <meta name="description" content="Sonoj">
    <meta property="og:site_name" content="Sonoj"/>
    <meta property="og:title" content="Sonoj"/>
    <meta property="og:description" content="Sonoj"/>
    <meta property="og:url" content="https://www.sonoj.org">
    <meta property="og:type" content="community"/>

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" type="image/png" href="favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="mstile-144x144.png">


    <link rel="stylesheet" type="text/css" href="style.css" />

</head>
<body>

<header>

<div class="logo">
    <img src="logo.png" alt="Logo">
</div>

<div class="intro-text-container">
   Welcome to Sonoj, a community dedicated to making music and sounds using free and
   shareable software. <br> This is a place where you can find friends, help, share knowledge,
   and talk about music, audio, tech, and software.
</div>

</header>

<main>

<div class="intro-text-container accounts">
    <?php
    if ($userLoggedIn) {
        // User is logged in, show the welcome message
        echo "Welcome back, " . htmlspecialchars($_SESSION['user_name']) . "!<br>";
        echo '<a href="https://accounts.sonoj.org/realms/sonoj/account">Account Settings</a> or ';
        echo '<a href="logout.php">Logout</a>';
    } else {
        try {
            // Attempt silent authentication to see if the user is already logged in
            $oidc->addAuthParam(['prompt' => 'none']);
            $oidc->authenticate();

            // If authentication is successful, retrieve user information
            $userInfo = $oidc->requestUserInfo();

            // Store user information and tokens in the session and cookies
            $_SESSION['user_logged_in'] = true;
            $_SESSION['user_name'] = $userInfo->preferred_username;
            $_SESSION['id_token'] = $oidc->getIdToken(); // Store the ID token in the session
            $_SESSION['access_token'] = $oidc->getAccessToken(); // Store the access token in the session

            // Set cookies for persistence
            setLoginCookie('user_logged_in', 'true');
            setLoginCookie('user_name', $userInfo->preferred_username);

            // Redirect to the same page without query parameters
            header('Location: /');
            exit();
        } catch (Exception $e) {
            // If the user is not logged in, show the login button
            echo 'We use a common "Sonoj Account" for our Chat, Forum and more. <br>';
            echo '<a href="login.php">CREATE or LOGIN to your Sonoj Account </a>';
        }
    }
    ?>
</div>

<div class="app">
    <!-- Card Start -->
    <div class="card" style="background-color: #faffff;">
        <a href="https://sonoj.org/2025/" class="card-link">
            <img src="card-sonoj.jpg" alt="Image Sonoj Convention">
            <div class="container">
                <h3>Sonoj Convention</h3>
                <h5>October 18th and 19th 2025</h5>
                <p>Annual event in Cologne, Germany, focusing on music production with free and open source software.</p>
            </div>
            <div class="ribbon">Real Life</div>
        </a>
    </div>
    <!-- Card End -->

    <!-- Card Start -->
    <div class="card" style="background-color: #faffff;">
        <a href="https://chat.sonoj.org" class="card-link">
            <img src="card-chat.jpg" alt="Image Chat">
            <div class="container">
                <h3>Chat</h3>
                <h5>Private Matrix Server</h5>
                <p>Chat for meeting friends, short-term discussions, collaborations, online-events and a news ticker. Uses our common Sonoj Account.</p>
            </div>
        </a>
    </div>
    <!-- Card End -->

    <!-- Card Start -->
    <div class="card" style="background-color: #faffff;">
        <a href="https://forum.sonoj.org/" class="card-link">
            <img src="card-forum.jpg" alt="Image Forum">
            <div class="container">
                <h3>Forum</h3>
                <h5>Flarum</h5>
                <p>Forum for medium-term discussions, cooperations and the search for help with music and software. Uses our common Sonoj Account.</p>
            </div>
        </a>
    </div>
    <!-- Card End -->


    <!-- Card Start -->
    <div class="card" style="background-color: #faffff;">
        <a href="https://doc.sonoj.org/new" class="card-link">
            <img src="card-documents.jpg" alt="Image Documentation">
            <div class="container">
                <h3>Documents</h3>
                <h5>Hedgedoc</h5>
                <p>Platform for long-term collaborative writing, slideshows, planning, guides and taking notes. Uses our common Sonoj Account.</p>
            </div>
        </a>
    </div>
    <!-- Card End -->

    <!-- Card Start -->
    <div class="card" style="background-color: #faffff;">
        <a href="https://sonoj.org/archive/" class="card-link">
            <img src="card-archive.jpg" alt="Image Archive">
            <div class="container">
                <h3>Convention Archive</h3>
                <h5>&nbsp;</h5>
                <p>Summaries of past conventions and links to the original websites.</p>
            </div>
        </a>
    </div>
    <!-- Card End -->

    <!-- Card Start -->
    <div class="card" style="background-color: #faffff;">
        <a class="card-link" rel="me" href="https://musicworld.social/@sonoj">
            <img src="card-mastodon.jpg" alt="Mastodon Logo">
            <div class="container">
                <h3>Mastodon Account</h3>
                <h5>Micro-Blogging</h5>
                <p>Our Account on Mastodon: "Social networking that's not for sale."</p>
            </div>
        </a>
    </div>
    <!-- Card End -->

    <!-- Card Start -->
    <div class="card" style="background-color: #faffff;">
        <a href="https://osamc.de" class="card-link">
            <img src="card-osamc.jpg" alt="OSAMC">
            <div class="container">
                <h3>Open Source Audio Meeting Cologne</h3>
                <h5>&nbsp;</h5>
                <p>Monthly meetup in Cologne every third Wednesday of the month (in German language).</p>
            </div>
            <div class="ribbon">Real Life</div>
        </a>
    </div>
    <!-- Card End -->



    </div> <!-- app end -->

</main>

<footer class="legal-imprint container">
    <p>Verantwortlich für den Inhalt:</p>
    <p>Nils Hilbricht, c/o RA Matutis</p>
    <p>Berliner Straße 57, 14467 Potsdam, Germany</p>
    <p>E-mail: <a href="mailto:info@sonoj.org">info@sonoj.org</a></p>
    <p>Tel: <a href="tel:+4922198657450">+49 221 98657450</a></p>
</footer>

</body>

<script>
// Remove query parameters from the URL without reloading the page
if (window.history.replaceState) {
    const cleanURL = window.location.protocol + "//" + window.location.host + window.location.pathname;
    window.history.replaceState({ path: cleanURL }, '', cleanURL);
}
</script>

</html>
