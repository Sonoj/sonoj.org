/*
 * Registration Form
*/

$('#registration-form').submit(function(e){
    e.preventDefault();

    var postForm = { //Fetch form data
            'fname'     : $('#registration-form #fname').val(),
            'email'     : $('#registration-form #email').val(),
            //'freetext'     : $('#registration-form #freetext').val(),
    };

    $.ajax({
            type      : 'POST',
            url       : './php/visitor-registration.php',
            data      : postForm,
            dataType  : 'json',
            success   : function(data) {
                            if (data.success) {
                                //alert("This is alert box!");
                                //$('.subscribe').empty();
                                $('.subscribe').empty();
                                $('.subscribe').html("<h1> Registration Successful.</h1>");
                                //$('#subscribe').removeClass("alert-danger");
                                //$('#subscribe').addClass("alert-success");
                                //$('#subscribe').show();
                            }
                            else
                            {
                                $('.subscribe').empty();
                                $('.subscribe').html("<h1> Registration Failed. Please try again with correct name and e-mail or write directly to <a href=\"mailto:info@sonoj.org\">mailto:info@sonoj.org</a></h1>");
                                //$('#subscribe .alert').html("Registration Failed. Please write directly to info@sonoj.org");
                                //$('#subscribe .alert').removeClass("alert-success");
                                //$('#subscribe .alert').addClass("alert-danger");
                                //$('#subscribe').show();
                            }
                        }
        });
});
