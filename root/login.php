<?php
// Start a session
session_start();

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

use Jumbojett\OpenIDConnectClient;

$oidc = new OpenIDConnectClient(
    $oidc_url,
    $oidc_client_id,
    $oidc_client_secret
);

// Set the redirect URL after login
$oidc->setRedirectURL($oidc_redirect_url);

// Authenticate the user (this will redirect to Keycloak login page if not authenticated)
$oidc->authenticate();

// After successful authentication, retrieve user information
$userInfo = $oidc->requestUserInfo('preferred_username');

// Store user information and tokens in the session
$_SESSION['user_logged_in'] = true;
$_SESSION['user_name'] = $userInfo;
$_SESSION['id_token'] = $oidc->getIdToken(); // Store the ID token in the session
$_SESSION['access_token'] = $oidc->getAccessToken(); // Optionally store the access token if needed

// Function to set a secure, long-lived cookie
function setLoginCookie($name, $value, $expire = 86400 * 30) { // 30 days
    setcookie($name, $value, time() + $expire, "/", "", true, true);
}

// Set cookies for persistence
setLoginCookie('user_logged_in', 'true');
setLoginCookie('user_name', $userInfo);

// Redirect to the homepage
header('Location: /');
exit();
?>
