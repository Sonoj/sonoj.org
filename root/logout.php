<?php
// Start a session
session_start();

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

use Jumbojett\OpenIDConnectClient;

$oidc = new OpenIDConnectClient(
    $oidc_url,
    $oidc_client_id,
    $oidc_client_secret
);

// Set the redirect URL after logout
$oidc->setRedirectURL($oidc_redirect_url);

// Function to clear cookies
function clearLoginCookies() {
    setcookie('user_logged_in', '', time() - 3600, "/");
    setcookie('user_name', '', time() - 3600, "/");
}

// Check if the ID token is set in the session
if (isset($_SESSION['id_token'])) {

    $tokencopy = $_SESSION['id_token'];
    $_SESSION = []; // Unset all session variables

    // Destroy the session cookie
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }

    session_destroy(); // Destroy the session

    // Clear the login cookies
    clearLoginCookies();

    // Perform the logout operation using the ID token
    $oidc->signOut($tokencopy, $oidc_redirect_url);
    // Code after this line will not get called because signout already redirects.
} else {
    // If no ID token is found, log the error and show a message
    error_log("Logout attempt without a valid session or ID token.", 0);

    // Clear the login cookies
    clearLoginCookies();

    // Redirect to the homepage after displaying the error
    header('Location: /');
    exit();
}
?>
