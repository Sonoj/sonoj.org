<?php
declare(strict_types=1);

session_start();

header('Content-Type: application/json');

$form_data = [];

try {
    // Define the path to the database file outside the document root
    $dbFilePath = '/home/mvsoyrjv/sonoj-visitor-registrations.sqlite';

    // Hardcoded year
    $conventionYear = '2024'; // Update this value each year

    // Resolve the real paths
    $dbRealPath = realpath($dbFilePath);
    $dirRealPath = realpath(__DIR__);

    // If the database file doesn't exist yet, resolve the absolute path
    if ($dbRealPath === false) {
        $dbDirRealPath = realpath(dirname($dbFilePath));
        if ($dbDirRealPath === false) {
            throw new Exception('Database directory does not exist.');
        }
        $dbRealPath = $dbDirRealPath . DIRECTORY_SEPARATOR . basename($dbFilePath);
    }

    // Ensure the database file path is outside the document root
    if (strpos($dbRealPath, $dirRealPath . DIRECTORY_SEPARATOR) === 0) {
        throw new Exception('Database file path must be outside the document root for security reasons.');
    }

    // Connect to the SQLite database
    $pdo = new PDO('sqlite:' . $dbFilePath);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Create a table for registrations if it doesn't already exist
    $pdo->exec("
        CREATE TABLE IF NOT EXISTS registrations (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            email TEXT NOT NULL,
            registration_date DATETIME NOT NULL,
            ip_address TEXT NOT NULL,
            user_agent TEXT NOT NULL,
            convention_year TEXT NOT NULL
        )
    ");

    // Retrieve and sanitize user inputs
    $fname = $_POST['fname'] ?? '';
    $email = $_POST['email'] ?? '';

    $fname = trim($fname);
    $email = trim($email);

    // Validate inputs
    if (empty($fname) || empty($email)) {
        $form_data['success'] = false;
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $form_data['success'] = false;
    } else {
        // Collect meta-information
        $registrationDate = date('Y-m-d H:i:s');
        $ipAddress = $_SERVER['REMOTE_ADDR'] ?? 'Unknown';
        $userAgent = $_SERVER['HTTP_USER_AGENT'] ?? 'Unknown';

        // Insert registration data into the database
        $insertStmt = $pdo->prepare("
            INSERT INTO registrations (
                name, email, registration_date, ip_address, user_agent, convention_year
            ) VALUES (
                :name, :email, :registration_date, :ip_address, :user_agent, :convention_year
            )
        ");
        $insertStmt->bindParam(':name', $fname, PDO::PARAM_STR);
        $insertStmt->bindParam(':email', $email, PDO::PARAM_STR);
        $insertStmt->bindParam(':registration_date', $registrationDate, PDO::PARAM_STR);
        $insertStmt->bindParam(':ip_address', $ipAddress, PDO::PARAM_STR);
        $insertStmt->bindParam(':user_agent', $userAgent, PDO::PARAM_STR);
        $insertStmt->bindParam(':convention_year', $conventionYear, PDO::PARAM_STR);

        if ($insertStmt->execute()) {
            $form_data['success'] = true;

            // Send email confirmation to the user
            $to = $email;
            $subject = "Sonoj $conventionYear Registration Confirmation";
            $safeName = htmlspecialchars($fname, ENT_QUOTES, 'UTF-8');
            $message = "Dear $safeName,\n\nThank you for registering for $conventionYear. We look forward to seeing you!\n\nBest regards,\nThe Sonoj Team";
            $headers = "From: info@sonoj.org\r\n";
            $headers .= "Reply-To: info@sonoj.org\r\n";
            $headers .= "Content-Type: text/plain; charset=utf-8\r\n";
            $headers .= "MIME-Version: 1.0\r\n";

            // Use mail() function to send the email
            mail($to, $subject, $message, $headers);

            // Optionally, continue sending the email notification to the admin
            $adminEmail = 'info@sonoj.org';
            $adminSubject = "New $conventionYear Registration: $fname";
            $adminMessage = "Name: $fname\nEmail: $email\nRegistration Date: $registrationDate\nIP Address: $ipAddress\nUser Agent: $userAgent";
            mail($adminEmail, $adminSubject, $adminMessage, $headers);
        } else {
            $form_data['success'] = false;
        }
    }
} catch (Exception $e) {
    // Handle any errors
    $form_data['success'] = false;
    // Optionally, log the error or include an error message in the response
    error_log($e->getMessage());
    $form_data['error'] = 'Error: ' . $e->getMessage();
}

// Return JSON response
echo json_encode($form_data);
?>
