#!/bin/bash

if [[ -z "$1" ]]; then
    echo "Must provide a year as parameter" 1>&2
    exit 1
fi

YEAR=$1

ls src/$YEAR/pre.*.html | entr sh generate.sh $YEAR &
ENTR=$!
php -S 0.0.0.0:8000 -t build/$YEAR
kill $ENTR
